import React, { Component } from 'react';

class MovieResult extends Component {
    render() {
        return (
            <div>
                <ul>
                  <li>
                  1º {this.props.result.champion.title}
                  </li>
                  <li> 
                  2º  {this.props.result.viceChampion.title}      
                  </li>
                </ul>
            </div>
        );
    }
}

export default MovieResult;