import React from 'react';
import  ListMovies from './components/ListMovies';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
       <ListMovies></ListMovies>
      </header>
    </div>
  );
}

export default App;
