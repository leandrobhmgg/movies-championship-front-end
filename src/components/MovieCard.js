import React, { Component } from 'react';

import { Card } from 'react-bootstrap';

class MovieCard extends Component {


    handleAllChecked(event){
        
        this.props.handleAllChecked(event);
    }


    render() {
        return (

            <Card style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title>{this.props.movie.title}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{this.props.movie.title}</Card.Subtitle>
                <Card.Subtitle className="mb-2 text-muted">
                {this.props.movie.year}
                </Card.Subtitle>
                <input 
                    key={this.props.movie.id} 
                    value={this.props.movie.id} 
                    type="checkbox" 
                    onChange={this.handleAllChecked.bind(this)}   />
            </Card.Body>
            </Card>

        );
    }
}

export default MovieCard;