import React, { Component } from 'react';
import MovieCard from './MovieCard';
import MovieResult from './MovieResult';

export default class ListMovies extends Component {
    

    constructor() {
        super()
        this.state = {
             movies: [],
             ids : [],
             count : 0,
             enableButton : false,
             result : null
         }
      }

      componentDidMount() {
        var myRequest = new Request("https://localhost:5001/api/movie");
        let movies = [];
    
        fetch(myRequest)
          .then(response => response.json())
          .then(data => {
            this.setState({ movies: data })
          })
      }

      handleAllChecked = (event) => {
        
      let ids = this.state.ids;

      if (ids.length >= 8)
      {
        alert("apenas 8 filmes podem ser selecionados");
        event.target.checked = false;
      }
      else{

            if(event.target.checked)
            ids.push(event.target.value);
            else
            ids = ids.filter(x=>x != event.target.value )


            if(ids.length <= 8)
            this.setState({
            ids: ids,
            count : ids.length,
            enableButton:   ids.length == 8 ? true : false
            });
      }

      }

      handlePlay = (event) => {
        
        var parameter = {
          "ids": this.state.ids
        };

        fetch("https://localhost:5001/api/movie/play",
        {
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(parameter)
        }).then(response => response.json())
          .then(data => {

            var state = this.state;
            state.result = data;
            this.setState(state);
   
          })
      }

      render() {

        if(this.state.result != null)
        {           
          return (<MovieResult result ={this.state.result}/>)
        }
       else
        {
        return (

            <div>
            <h1>Movie List</h1>
     
          <div className="row">                 
              {this.state.movies.map(movie => {
               return <MovieCard 
                         movie={movie}
                         handleAllChecked={this.handleAllChecked.bind(this)}             
                      />               
              })}
           </div>
      
           <button 
           style={{display:(this.state.enableButton? 'block':'none')}}
           onClick={this.handlePlay}
           >
             GERAR MEU CAMPEONATO
           </button>
          </div>
        );
      }
    }
}
